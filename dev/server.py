#!/usr/bin/env python
#coding=utf-8
import subprocess
from itertools import filterfalse
from heapq import heapify, heappush, heappop

SMNUM = 512
eps = 1e-6

status = []  # [ (appid,thx.id,SM-id) ]
ctime = 0  # update on evolution
job_queue = []  # [ [ appid,thx_num]]
actionQueue = []
requestQueue = []
totalTime = []
affectedThreads = {}
#runningThread = []  # appid -> #threads running
#remThread = []  # appid -> #remThreads


class Thread:
    def __init__(self, appid, tid, remTime):
        self.appid = appid
        self.tid = tid
        self.remTime = remTime

    def assign2SM(self, sid):
        self.assignedTo = sid

    def toString(self):
        return str(self.appid) + "@" + str(self.tid)


def partition(p, x):
    return list(filter(p, x)), list(filterfalse(p, x))


def updateCap():
    for sm in status:
        sharingThreads = len(sm)
        for t in sm:
            t.cap = 1.0 / sharingThreads


def parsePart(x):
    [sid, y] = x.split('/')
    [appid, tid] = y.split('@')
    return (int(sid), int(appid), int(tid))


def evolveHelper(delta):
    updateRequestQueue(delta)
    global ctime
    ctime += delta


def initRemTime(appid):
    return totalTime[appid]


def doMV(r, src, dst):
    (sid, appid, tid) = parsePart(src)
    dsid = int(dst)
    global status
    a, b = partition(lambda x: x.tid == tid, status[sid])
    try:
        status[dsid].append(a[0])
    except IndexError:
        print(len(requestQueue))
        print(dsid)
        print(tid)
        raise IndexError
    status[sid] = list(b)
    if tid in affectedThreads:
        affectedThreads[tid] = affectedThreads[tid] + 1
    else:
        affectedThreads[tid] = 1


def doTouch(r, x):
    (sid, appid, tid) = parsePart(x)
    global status, affectedThreads
    affectedThreads[tid] = r + 1
    status[sid].append(Thread(appid, tid, initRemTime(appid)))


def doAction():
    global actionQueue
    for r, transaction in enumerate(actionQueue):
        for cmd in transaction:
            scmd = cmd.split()
            if scmd[0] == "mv":
                doMV(r, scmd[1], scmd[2])
            elif scmd[0] == "touch":
                doTouch(r, scmd[1])
            else:
                print(scmd[0])
                raise Exception("Unknown Command")
    actionQueue = []
    updateCap()


def doRequest(cin):
    # TODO: group terminating threads together?
    global requestQueue, actionQueue, changed
    if requestQueue[0][0] > ctime:
        return
    changed = True
    (etime, e, cmd) = heappop(requestQueue)
    bufOut = str(e) + " " + cmd + "\n"
    if e == 1:
        bufOut = [cmd]
        while requestQueue[0][1] == 1 and requestQueue[0][0] < ctime:
            (etime, e, cmd) = heappop(requestQueue)
            bufOut.append(cmd)
        cin.write(str(1) + '\n')
        cin.write(str(len(bufOut)) + '\n')
        cin.write('\n'.join(bufOut) + '\n')
        cin.flush()
    else:
        cin.write(bufOut)
    #p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE, close_fds=True)
    #outs = p.communicate()[0].decode('utf-8').splitlines()
    outs = (subprocess.check_output(schedulerCmd, shell=True, universal_newlines=True)).splitlines()
    t = 0
    try:
        t = int(outs.pop(0))
    except:
        print("zzz")
        print((subprocess.check_output(schedulerCmd, shell=True, universal_newlines=True)))
        raise Exception
    for i in range(0, t):
        n = int(outs.pop(0))
        tmpQ = []
        for j in range(0, n):
            tmpQ.append(outs.pop(0))
        actionQueue.append(tmpQ)


def evolve(cin):
    global changed, affectedThreads
    affectedThreads = {}
    delta = len(actionQueue)
    if actionQueue:
        changed = True
        doAction()
    elif requestQueue:
        doRequest(cin)
        delta = 1.0
    else:
        delta = 10.0
    evolveHelper(delta)


# update remTime and append request if necessary
def updateRequestQueue(delta):
    global status, changed
    for sm in status:
        for t in sm:
            if t.tid in affectedThreads:
                t.remTime -= (delta - affectedThreads[t.tid]) * t.cap
            else:
                t.remTime -= delta * t.cap
            if t.remTime < eps:
                heappush(requestQueue, (ctime, 1, str(t.tid)))
                changed = True
    status = list(map(lambda x: list(filter(lambda y: y.remTime > -eps, x)), status))


def initRequest():
    global requestQueue, totalTime
    heappush(requestQueue, (10, 0, "64 256"))
    heappush(requestQueue, (250, 0, "64 256"))
    heappush(requestQueue, (500, 0, "64 256"))
    heappush(requestQueue, (750, 0, "64 256"))
    heappush(requestQueue, (1000, 0, "64 256"))
    heappush(requestQueue, (5000, 0, "64 256"))
    heappush(requestQueue, (15000, 0, "64 256"))
    heappush(requestQueue, (20000, 0, "64 512"))
    totalTime = list(range(1000, 1010))
    totalTime[0] = 1000
    totalTime[1] = 2000
    totalTime[2] = 3000
    totalTime[3] = 4000
    totalTime[4] = 5000
    totalTime[5] = 6000
    totalTime[6] = 7000
    totalTime[7] = 8000
    totalTime[8] = 9000


def dumpStatus():
    print()
    print(ctime)
    for sm in status:
        out = "["
        for t in sm:
            out += t.toString() + ","
        print(out + "]")


if __name__ == "__main__":
    for i in range(0, SMNUM):
        status.append([])
    actionQueue = []
    heapify(requestQueue)
    initRequest()
    schedulerCmd = "./scheduler |tee s.log"
    #(cin, cout) = (p.stdin, p.stdout)
    cin = open("./hist", "w")
    cin.write(str(SMNUM) + "\n")
    cin.flush()
    while True:
    #while ctime < 510:
        changed = False
        evolve(cin)
        if changed:
            dumpStatus()
