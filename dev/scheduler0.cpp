#include<iostream>
#include<list>
#include<vector>
#include<map>
#include<algorithm>
#include<functional>
#include<cstdlib>
#include<cstring>
using namespace std;
// input:
// M: #-SMs
// foreach round:
// e: type of event: 0: new-application 1:done-threads
// --------------------
// Sample:
// 0
// 0 5 10 //app-id min-req opt-req
// --------------
// 1
// 2 //# of affected apps
// 0 4 8//app-id new-min-req new-opt-req
// 1 4 8//app-id new-min-req new-opt-req
//
// output:
// t: #-transactions
// n: #-actions-per-transaction
// mv SM-id/thread-id SM-id
// rm SM-id/thread-id
//
// status:
// SM-id -> [threads] : status
// thread-id -> SM-id : getSMById
// app-id -> min-req: getMinById
// app-id -> opt-req: getOptById
// app-id -> current-SMs: getCntById
class Application{
	public:
		int id;
		int SMcnt;
		static int cumid;
		static int living;
		
		Application(){
			id=cumid;
			cumid++;
			living++;
			SMcnt=0;
		}
		Application(int z){
			id=z;
			cumid=max(cumid,z+1);
			living++;
			SMcnt=0;
		}
		~Application(){
			living--;
		}
		//MAYBE_TODO:get whole list of Threads
};
class Thread{
	public: 
		Application *appid;
		int id;
		static int cumid;
		
		Thread(int z,Application &a){
			id=z;
			appid=&a;
			appid->SMcnt++;
		}
		Thread(Application &a){
			appid=&a;
			id=cumid++;
			appid->SMcnt++;
		}
		~Thread(){
			appid->SMcnt--;
		}
};

// -----------------------------
// -----------------------------
int M;
vector< list<Thread> > status;
map<int, int> getSMById;
map<int, int> getCntById;
map<int, int> getMinById, //min-req for particular app
			  getOptById, //opt-req for 
			  getAppById; //get appid by thread id

int Application::cumid=0;
int Thread::cumid=0;
void onNewApp(int a,int m,int o){
//if(enough segments)
//else
//pick an rich SM of a rich app
//try to merge some free segments to get more consecutive space
}
int main(){
	cin>>M;
	int e;
	while(cin>>e){
		if(e==0){
			int a,m,o;
			cin>>a>>m>>o;
			getMinById[a]=m;
			getOptById[a]=o;
			onNewApp(a,m,o);
		}else{
			int n;
			cin>>n;
			for(int i=0;i<n;i++){
				int id;
				cin>>id;
				list<Thread> &ts=status[getSMById[id]];
				ts.erase(remove_if(ts.begin(),ts.end(),[id](Thread &t) { return t.id==id;} ), ts.end());
				getSMById.erase(id);
			}
			cin>>n;
			for(int i=0;i<n;i++){
				int id,m,o;
				cin>>id>>m>>o;
				getMinById[id]=m;
				getOptById[id]=o;
			}
			//TODO:update getAppById,getCntById
		}
	}
	return 0;
}
