#include<iostream>
#include<functional>
#include<algorithm>
#include<vector>
#include<list>
#include<map>
#include<cstdlib>
#include<cstdio>
#include<sstream>
#include<string>
#include<cassert>
#include<iomanip>
#include<ctime>
#include<set>
#include<cstring>
#include<cmath>
#include<queue>
#include<cassert>
#include<utility>
#define ull unsigned long long
#define MP make_pair
#define pb push_back
#define FILE_TESTING 0
#ifndef INT_MAX
#define INT_MAX (1<<30)
#endif
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef vector<ii> vii;
typedef vector<vii> vvii;
template<class T,class S> inline S for_all(T &x, S f){ return for_each(x.begin(),x.end(),f);}
template<class T,class S> inline int count_all(T &x, S f){ return count_if(x.begin(),x.end(),f);}
template<class T,class S> inline S for_all_r(T &x, S f){ return for_each(x.rbegin(),x.rend(),f);}

string itos(ull t)
{
    string ret;
    char tmp[100];
    sprintf(tmp,"%lld",t);
    ret=tmp;
    return ret;
}

const double eps=1e-6;

int getCirDistance(int x, int y, int M){
	return min((x-y+M)%M,(y-x+M)%M);
}

