#include "mylib.h"
#include<fstream>
typedef list<int> Segment;
typedef list< Segment > SegList;
class Thread{
	public:
		int tid,appid;
		int assignedSM;
		static int cumid;
		Thread(int a){
			tid=cumid++;
			appid=a;
			assignedSM=-1;
		}
		friend void assignToSM(Thread *t,int a);
		friend void simpleMakeRoom(const int, const int);
		friend void simpleContract(Segment &, int, int &, double);
		string toString(){
			stringstream ret;
			//ret<<assignedSM<<":"<<appid<<"/"<<tid;
			ret<<appid<<"@"<<tid;
			return ret.str();
		}
};
typedef list<Thread *> TList;
class Application{
	public:
		int appid;
		int minReq,optReq;
		int occupiedSM;
		static int cumid;
		TList tlist;
		//TODO: this seglist should be deleted? since ref to this can be more lightweight than update, using groupSegList()
		SegList segList;
		friend void simpleAdjust(Application *a,int m,int o);
		friend void simpleMakeRoom(const int, const int);
		friend void simpleContract(Segment &, int, int &, double);
		Application(int m,int o){
			appid=cumid++;
			minReq=m;
			optReq=o;
			occupiedSM=0;
		}
		bool isRich(){ return occupiedSM>minReq; }
		bool canSegment(){ return segList.size() < minReq; }
		int getMaxSeg(){ assert(segList.size()<=minReq); return minReq - segList.size(); }
		string toString(){
			stringstream ret;
			ret<<appid<<":{min="<<minReq<<", opt="<<optReq<<", occupiedSM="<<occupiedSM<<", #segments="<<segList.size()<<", #threads="<<tlist.size()<<"},\t ";
			return ret.str();
		}
		bool isPromotable(bool hasNeighbor){ return this->canSegment()||(hasNeighbor&&optReq>occupiedSM); }
		int getMaxMoreThreads(){
			//TODO: maybe related to opt. req.
			return INT_MAX;
		}
};
map<int, Application *> appPool;
map<int, Thread *> thPool;
vector< TList > status;
vector< int > readyOnRound;
//TODO: Optimize: maintain more global structures, sorted by load/stress or whatever.
int M;
int Application::cumid=0;
int Thread::cumid=0;
vector< list<string> > transactions;
ofstream slog;

template<typename Func>
SegList groupCircleBy(Func f){
	SegList ret;
	bool head=false,tail=false,last=false;
	int i=0;
	for(auto x : status){
		if(f(x)){
			if(last){
				(ret.back()).pb(i);
			}else {
				Segment t;
				t.pb(i);
				ret.pb(t);
			}
			head= i==0;
			tail= i==M;
			last=true;
		}else{
			last=false;
		}
		i++;
	}
	if(head&&tail&&ret.size()>=2){
		for(auto x:ret.front())ret.back().pb(x);
		ret.pop_front();
	}
	return ret;
}

SegList groupEmptySM(){
	return groupCircleBy([](const list<Thread *> &x){return x.empty();});
}

SegList groupSegList(int appid){
	return groupCircleBy([appid](const list<Thread *> &x){return !x.empty()&&(x.front())->appid==appid;});
}

SegList groupSegList(){
	SegList ret;
	int i=0;
	//list<Thread*> *last=NULL;
	int last=-2;
	for(auto x : status){
		if((last==-1&&x.empty())||(!x.empty()&&last==(x.front())->appid)){
			(ret.back()).pb(i);
		}else {
			Segment t;
			t.pb(i);
			ret.pb(t);
			last=x.empty()?-1:(x.front())->appid;
		}
		i++;
	}
	if(ret.size()>=2&&((status[0].empty()&&status[M-1].empty())||
				(!status[0].empty()&&!status[M-1].empty()&&
				 status[0].front()->appid==status[M-1].front()->appid))){
		for(auto x:ret.front())ret.back().pb(x);
		ret.pop_front();
	}
	return ret;

}

int getDistance(Segment &s, int id){
	int l=s.front(),r=s.back();
	return min(getCirDistance(l, id, M),getCirDistance(r, id, M));
}

void assignToSM(Thread *t,int a){
	assert(status[a].empty()||(*status[a].begin())->appid==t->appid);
	if(status[a].empty()&&(t->assignedSM<0||!status[t->assignedSM].empty())){
		appPool[t->appid]->occupiedSM++;
	}
	t->assignedSM=a;
	status[a].pb(t);
}

void emit(int round,string s){
	int todo=round+1-transactions.size();
	for(int i=0;i<todo;i++){
		list<string> tmp;
		transactions.pb(tmp);
	}
	transactions[round].pb(s);
}

//update Thread::assignedSM, status.  
void emitMV(int round, int fromSmId, int tid, int toSmId){
	assert(abs(fromSmId-toSmId)==1||abs(fromSmId-toSmId)==M-1);
	//status[fromSmId].remove(thPool[tid]);
	thPool[tid]->assignedSM=toSmId;
	status[toSmId].pb(thPool[tid]);
	emit(round,"mv "+itos(fromSmId)+"/"+thPool[tid]->toString()+ " "+ itos(toSmId));
}

void spawnThread(Segment &s, Application *a, int o){
	Segment z;
	for(auto y : s){
		if(a->tlist.size()>=o)break;
		Thread *newt= new Thread(a->appid);
		thPool[newt->tid]=newt;
		assignToSM(newt,y);
		a->tlist.pb(newt);
		z.pb(y);
		emit(readyOnRound[y],"touch "+itos(y)+"/"+newt->toString());
	}
	//a->segList.pb(z);
	a->segList=groupSegList(a->appid);
}

//simple-re-balance
//NOTE: lots of magical number here.....
//TODO: combine re-balance and contraction together, consider each thread, first move the necessary part, then continue moving as long as the load condition holds
void simpleRebalance(int l, int r, int target, int round, double load){
	for(int curRound=0;curRound<round;curRound++){
		for(int p=target;p>=l;p--){
			int pp=(p+M)%M;
			int np=(pp+1)%M;
			int curLoad=status[np].size();
			while(!status[pp].empty()&&curLoad*1.1<load){
				emitMV(curRound,pp,(*status[pp].begin())->tid,np);
				status[pp].pop_front();
				curLoad++;
			}
		}
		for(int p=target+1;p<=r;p++){
			int pp=(p+M)%M;
			int np=(pp+M-1)%M;
			int curLoad=status[np].size();
			while(!status[pp].empty()&&curLoad*1.1<load){
				emitMV(curRound,pp,(*status[pp].begin())->tid,np);
				status[pp].pop_front();
				curLoad++;
			}
		}
	}
}

void simpleContract(Segment &s, int cnt, int &maxSegCnt, double load){
	int todo=int(s.size())-cnt;
	if(todo==0)return;
	assert(todo>0);
	int l=*(s.begin()),r=*(s.rbegin());
	if(l>r)l-=M;

	//try further segment
	if(maxSegCnt>0&&s.size()>=8&&todo>=4&&cnt>=2){
		//first calculate the mass centroid.
		double p=0,q=0;
		int j=l;
		for(auto id:s){
			int tmp=status[id].size();
			q+=tmp;
			p+=j*tmp;
			j++;
		}
		assert(j==r+1);
		int centroid=floor(p/q);
		j=0;
		//now segment at the centroid.
		Segment ls,rs;
		for(j=l;j<=centroid;j++)ls.pb((j+M)%M);
		for(j=centroid+1;j<=r;j++)rs.pb((j+M)%M);
		maxSegCnt--;
		int lsegCnt=maxSegCnt>>1,rsegCnt=maxSegCnt-lsegCnt;
		int lcnt=cnt>>1,rcnt=cnt-lcnt;

		//recursive contraction
		simpleContract(ls,lcnt,lsegCnt,load);
		simpleContract(rs,rcnt,rsegCnt,load);

		maxSegCnt=lsegCnt+rsegCnt;
		return;
	}
	//do rebalance
	//TODO: if only 2 SMs in a segment could get very unbalance,especially reducing from 3 to 2 SMs, maybe try with centroid
	
	if(todo>=2&&cnt>=5){
		int round=max(((todo+1)>>1) - 1,1);
		simpleRebalance(l,r,(l+r)>>1,round,load);
	}
	//begin contraction
	int i=0;
	bool flag=(rand()%2==0);
	for(auto id: s){
			if(flag){
				if(i*2>=todo)break;
			}else{
				if(i*2>=todo-1)break;
			}
			for_all(status[id],[&](Thread *t){emitMV(i,id,t->tid,(id+1)%M);});
			status[id].clear();
			//should this be i or i+1?
			readyOnRound[id]=i;
			i++;
	}
	todo-=i;
	i=0;
	for_all_r(s,[&](const int id){
			if(i>=todo)return;
			for_all(status[id],[&](Thread *t){emitMV(i,id,t->tid,(id+M-1)%M);});
			status[id].clear();
			//should this be i or i+1?
			readyOnRound[id]=i;
			i++;
	});
}

//Possible Strategies:
//#1: smallest application living time 
//#2:*smallest tlist.size()/occupiedSM  
//#3: compose #1 and #2
//
//*1:*take 1 by 1    
//*2: compute some kind of fraction for each
//*3: if it's necessary to make fewer segments,then pick those closest to XXX first(introduce some distance for each SM).
//
//update: status, Thread::assignedSM, segList, occupiedSM
void simpleMakeRoom(const int cnt, const int exception){
	list<Application *> cand;//victim candidates
	for(auto x : appPool){
		Application *y=x.second;
		if(!y->isRich()||y->appid==exception)continue;
		cand.pb(y);
	}
	cand.sort([](Application *a,Application *b){return double(a->tlist.size())*double(b->occupiedSM) < double(b->tlist.size())*double(a->occupiedSM);});
	int cntFreed=0;
	for(auto x: cand){
		//TODO:optimize here, maybe separate Segment out and maintain something.
		if(cntFreed>=cnt)break;
		priority_queue< pair<double, pair<int, Segment> > > y;

		double totFrac=0;
		for(auto a : x->segList){
			double s1=0;
			for_all(a,[&](const int z){s1+=status[z].size();});
			y.push(MP(s1,MP(1,a)));
			totFrac+=s1/a.size();
		}
		int targetSM=max(x->minReq,x->occupiedSM-cnt+cntFreed);//one can change this for more based on rule *2
		
		int curMaxSeg=x->getMaxSeg();

		//use a heap to decide how many SMs need to be freed for each segment. complexity: O( curMaxFree * log(#Segments) )
		int curcnt=(x->segList).size();
		for(;curcnt<targetSM;){
			auto tmp=y.top();
			double load=tmp.first;
			int tmpSMs=tmp.second.first;
			y.pop();
			if(tmpSMs<(tmp.second.second).size()){
				y.push(MP(load*tmpSMs/(tmpSMs+1),MP(tmpSMs+1,tmp.second.second)));
				curcnt++;
			}
		}

		//next begin contraction 
		while(!y.empty()){
			auto tmp=y.top();
			y.pop();
			simpleContract(tmp.second.second,tmp.second.first,curMaxSeg,tmp.first);
		}

		cntFreed+=x->occupiedSM - targetSM;
		x->occupiedSM=targetSM;
		x->segList=groupSegList(x->appid);
	}
}

void simpleAdjust(Application *a, int m, int o){
	//TODO
}

void simplePromote(){
	// Possible strategies:
	// #1:  First merge by spawning as much as possible ( #threads < MaxSpawnThreads ), then contract.
	//		Next spawn the rest candidates.
	//		Next pick the heaviest segment and do re-balance for 2 rounds.(note that it's not necessarily that they are neighbors if it's got really heavy)
	//		Note that for simplicity I further require MaxMoreThreads>=s.size() instead of MaxMoreThreads+2>=s.size(),
	//		Since otherwise it's more complicated to generate the new freeSegList.

	SegList freeSegList=groupEmptySM();
	freeSegList.sort([](const Segment &a, const Segment &b){return a.size()>b.size();});
	priority_queue<pair<double,Application *> > cand;//promote candidates
	set<int> candSet;
	for(auto x : appPool){
		Application *y=x.second;
		if(!y->isPromotable(false))continue;
		cand.push(MP(y->tlist.size()/double(y->occupiedSM),y));
		candSet.insert(y->appid);
	}
	
	while(!freeSegList.empty()){
		auto &s=freeSegList.front();
		int l=*(s.begin()),r=*(s.rbegin());
		int ll=(l+M-1)%M,rr=(r+1)%M;
		Application *a1,*a2;
		if(!status[ll].empty()){a1=appPool[(*status[ll].begin())->appid];}
		if(!status[rr].empty()){a2=appPool[(*status[rr].begin())->appid];}
		if(l>r)l-=M;
		//int centroid=((l+r)>>1);
		//TODO:change to a->optReq+2>=s.size() and do a contraction later.
		if(a1==a2&&a1->getMaxMoreThreads()>=int(s.size())&&a1->optReq-a1->occupiedSM>=int(s.size())){
			int maxSpawn=a1->tlist.size()+int(s.size());
			spawnThread(s,a1,maxSpawn);
			freeSegList.pop_front();
		}else if(!cand.empty()&&s.size()>=4){
			auto c=cand.top().second;
			int maxSpawn=int((c->tlist).size())+min(min(c->getMaxMoreThreads(),int(s.size())),c->optReq-c->occupiedSM);
			spawnThread(s,a1,maxSpawn);
			freeSegList.pop_front();
		}else {
			break;
		}
	}
	auto allSegList=groupSegList();
	double maxLoad=0;
	Segment *heaviestSeg=NULL;
	//TODO:first pick the heaviest and extend it to the closest segment
	freeSegList.clear();
	for(auto &s:allSegList){
		double loadS=0;
		for_all(s,[&](const int z){loadS+=status[z].size();});
		loadS/=s.size();
		if(loadS>maxLoad){
			maxLoad=loadS;
			heaviestSeg=&s;
		}
		if(loadS<eps)
			freeSegList.pb(s);
	}
	if(!heaviestSeg)return;
	int closest=INT_MAX;
	Segment *closestSeg=NULL;
	for(auto &s:freeSegList){
		//TODO
	}
	if(!closestSeg)return;
	//move it along
}

void simpleTerminateThread(int tid){
	Thread *t=thPool[tid];
	Application *a = appPool[t->appid];
	status[t->assignedSM].remove(t);
	if (status[t->assignedSM].empty()) {
		a->occupiedSM--;
	}
	a->tlist.remove(t);
	a->segList = groupSegList(t->appid);
	delete t;
	thPool.erase(tid);
}


void simpleTerminateApp(int appid){
	Application *a=appPool[appid];
	for(auto t: a->tlist){
			thPool.erase(t->tid);
			status[t->assignedSM].clear();
			readyOnRound[t->assignedSM]=0;
			delete t;
	}
	delete a;
	appPool.erase(appid);
}

void onNewApp(int min_req, int o){
	SegList freeSegList=groupEmptySM();
	freeSegList.sort([](const Segment &a, const Segment &b){return a.size()>b.size();});
	Application *a=new Application(min_req,o);
	appPool[a->appid]=a;
	int i=0;
	int topFreeSM=0;
	
	for(auto x: freeSegList){
		if(i>=min_req)break;
		topFreeSM+=x.size();
		i++;
	}
	if(topFreeSM<min_req){ //implies freeSegList.size()<min_req
		simpleMakeRoom(min_req-topFreeSM,-1);
		freeSegList=groupEmptySM();
		freeSegList.sort([](const Segment &a, const Segment &b){return a.size()>b.size();});
	}
	i=0;
	//TODO:should we try the largest segment first or the smallest segment first?
	for(auto &x : freeSegList){
		if(i>=min_req||a->tlist.size()>=o)break;
		spawnThread(x,a,o);
		i++;
	}
}

void init(){
	srand(time(NULL));
	for(int i=0;i<M;i++){
		TList tmp;
		status.pb(tmp);
		int z=0;
		readyOnRound.pb(z);
	}
}

void dumpstatus(){
	int i=0;
	slog<<endl<<endl;
	for(auto x: status){
			slog<<i<<":[";
			for(auto y: x){
				slog<<(y->toString())<<", ";
			}
			slog<<"]"<<endl;
			i++;
	}
	slog<<endl<<endl;
	for(auto x:appPool){
			slog<<((x.second)->toString())<<endl;
	}
}

void proceed(istream& fin, ostream& fout){
	fin>>M;
	int e;
	init();
	while(fin>>e){
		transactions.clear();
		for(int i=0;i<M;i++)readyOnRound[i]=status[i].empty()?0:-1;
		if(e==0){
			int m,o;
			fin>>m>>o;
			onNewApp(m,o);
		}else if(e==1){
			int n;
			fin>>n;
			for (int i = 0; i < n; i++) {
				int tid;
				fin>>tid;
				simpleTerminateThread(tid);
			}
			simplePromote();
		}else if(e==2){
			int a;
			fin>>a;
			simpleTerminateApp(a);
			simplePromote();
		}else {
			//simpleAdjust
			int t;
			fin>>t;
			//TODO:
		}
		fout<<transactions.size()<<endl;
		for_all(transactions,[&](const list<string> &x){
				fout<<x.size()<<endl;
				for_all(x,[&](const string &y){
					fout<<y<<endl;
				});
		});
		fout.flush();
		dumpstatus();
	}

}
int main(){
	//ofstream fout;
#define fin cin
#define fout cout
	ifstream hin("./hist");
	ifstream tin;
	slog.open("./slog");
	if(hin)
		proceed(hin,fout);
	else 
		proceed(fin,fout);
	slog.close();
	return 0;
}
