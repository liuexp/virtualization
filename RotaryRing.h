#pragma once

#include <vector>
#include "Rotary.h"

class RotaryRing : public Rotary {
public:
	RotaryRing();
	void SetSM(const int);
	virtual void Move(const int, const int, const int, int);
	virtual void Over();
	virtual std::vector<int> Status() const;
	virtual void Pull(const int);
	virtual void Performance() const;
	const static int Null = -1;
private:
	int n;
	std::vector<int> a;
	std::vector<int> b;
};
