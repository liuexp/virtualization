#pragma once

#include<sstream>

class Application{
	public:
		int appid;
		int minReq;
		int optReq;
		int occupiedSM;
		int load;
		//static int cumid;

		Application(int a, int m, int o, int l){
			appid=a;
			//cumid=std::max(a,cumid)+1;
			minReq=m;
			optReq=o;
			load=l;
			occupiedSM=0;
		}

		std::string toString(){
			std::stringstream ret;
			ret<<appid<<":{min="<<minReq<<", opt="<<optReq<<", now=\t"<<occupiedSM<<"}";
			return ret.str();
		}
};
//int Application::cumid = 0;
