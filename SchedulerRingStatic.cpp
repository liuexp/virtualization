#include <iostream>
#include "SchedulerRingStatic.h"
#include "R.h"
#include <cstdlib>

SchedulerRingStatic::SchedulerRingStatic() {
}

void SchedulerRingStatic::SetSM(const int n) {
	this->n = n;
	_load.assign(n, 0);
	_aff.assign(n, -1);
	_crt.assign(n, -1);
}

int& SchedulerRingStatic::crt(int m) {
	return _crt[sm(m)];
}

int SchedulerRingStatic::u(int a, int b) {
	for (; crt(--a) == -1 || crt(a) == b; );
	return a;
}

int SchedulerRingStatic::v(int a, int b) {
	for (; crt(++a) == -1 || crt(a) == b; );
	return a;
}

int p(int n) {
	return (n - (n & 1)) / 2 + 1;
}

int q(int n) {
	return (n - (n & 1)) / 2;
}

int SchedulerRingStatic::U(int a, int b) {
	return p(u(a, b) + a);
}

int SchedulerRingStatic::V(int a, int b) {
	return q(a + v(a, b));
}

int r(int n) {
	return rand() % n;
}

void SchedulerRingStatic::Push(const int appl, const int min, const int opt, const int amt) {
	Interrupt();
	int tmp = min;
	while (tmp) {
		int t = r(n);
		if (_crt[t] == -1) {
			_crt[t] = appl;
			--tmp;
		}
	};
	for (int i = 0; i < n; ++i)
		if (_crt[i] != -1 && _crt[i] != appl)
			Adjust(U(i, appl), V(i, appl), U(i), V(i));
	Over();
	for (int i = 0, tmp = -1; i < n; ++i)
		if (_crt[i] == appl) {
			int L = U(i), R = V(i);
			for (int j = L; j <= R; ++j)
				aff(j) = appl;
			Adjust(L, R, L, R, 0, amt / min + (int)(++tmp < amt % min));
		}
	Over();
}

void SchedulerRingStatic::Interrupt() {
	_load = R.Status();
	std::vector<int> L(n), R(n);
	for (int i = 0; i < n; ++i)
		if (_crt[i] != -1) {
			L[i] = U(i);
			R[i] = V(i);
		}
	for (int i = 0; i < n; ++i)
		if (_crt[i] != -1) {
			bool flag = true;
			for (int j = L[i]; flag && j <= R[i]; ++j)
				flag &= !load(j);
			if (flag) _crt[i] = -1;
			for (int j = L[i]; flag && j <= R[i]; ++j)
				aff(j) = -1;
		}
	for (int i = 0; i < n; ++i)
		if (_crt[i] != -1)
			Adjust(L[i], R[i], U(i), V(i));
	Over();
}

void SchedulerRingStatic::Performance() const {
	std::cerr << "Performance:\n";
	std::cerr << "Excelent\n\n";
	std::cerr.flush();
}
