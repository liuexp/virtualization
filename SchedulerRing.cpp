#include "SchedulerRing.h"
#include "R.h"
#include <algorithm>

SchedulerRing::SchedulerRing() {
}

void SchedulerRing::add(int rnd, int appl, int src, int tar, int amt) {
	std::vector<int> tmp(5);
	tmp[0] = rnd;
	tmp[1] = appl;
	tmp[2] = src;
	if (src != -1) {
		_load[src] -= amt;
	}
	tmp[3] = tar;
	if (tar != -1) {
		_load[tar] += amt;
		_aff[tar] = appl;
	}
	tmp[4] = amt;
	inst.push_back(tmp);
}

int SchedulerRing::sm(int m) {
	return m % n < 0 ? n + m % n : m % n;
}

int& SchedulerRing::aff(int m) {
	return _aff[sm(m)];
}

int& SchedulerRing::load(int m) {
	return _load[sm(m)];
}

void SchedulerRing::Adjust(int l, int r, int L, int R, int rounds, int amt) {
	if (l == L && r == R && rounds == 0 && amt == 0) return;
//	std::cerr << "Adjust [" << l << ", " << r << "] to [" << L << ", " << R << "] in " << rounds << " rounds and adding " << amt << " tasks.\n";
	int c = -1;
	while (++c < rounds || l != L || r != R) {
		// A naïve algorithm.
		if (l < L) {
			add(c, aff(l), sm(l), sm(l + 1), load(l));
			++l;
		} else if (l > L) {
			add(c, aff(l), sm(l), sm(l - 1), load(l) * 2 / 3);
			--l;
		}
		if (r < R) {
			add(c, aff(r), sm(r), sm(r + 1), load(r) * 2 / 3);
			++r;
		} else if (r > R) {
			add(c, aff(r), sm(r), sm(r - 1), load(r));
			--r;
		}
	}
	for (int i = L; amt && i <= R; ++i)
		add(c, aff(i), -1, sm(i), amt / (R - L + 1) + (int)(amt % (R - L + 1) < i - L));
}

void send(std::vector<int>& inst) {
	R.Move(inst[1], inst[2], inst[3], inst[4]);
}

void SchedulerRing::Over() {
	if (inst.empty()) return;
	std::sort(inst.begin(), inst.end());
	int c = 0;
	for (auto it = inst.begin(); it != inst.end();) {
		for (; it != inst.end() && it->front() == c; ++it) send(*it);
		R.Over();
		++c;
	}
	inst.clear();
}
