#pragma once

#include <vector>

class Scheduler {
public:
	Scheduler();
	virtual void SetSM(const int amount) = 0;
	virtual void Push(const int applID, const int minReq, const int optReq, const int amt) = 0;
	virtual void Interrupt() = 0;
	virtual void Performance() const = 0;
protected:
	int n; // # of SMs
};
