#pragma once

#include <vector>
#include "Scheduler.h"

class SchedulerRing : public Scheduler {
public:
	SchedulerRing();
	virtual void SetSM(const int amount) = 0;
	virtual void Push(const int applID, const int minReq, const int optReq, const int amt) = 0;
	virtual void Interrupt() = 0;
	virtual void Performance() const = 0;
protected:
	std::vector<int> _aff; // Affiliations of SMs
	std::vector<int> _load; // Workloads of SMs

	int sm(int m);
	int& aff(int m);
	int& load(int m);

	std::vector< std::vector<int> > inst; // Buffer of instructions

	/**
	 * @param	l, r	Initial interval [l, r].
	 * @param	L, R	Target interval [L, R].
	 * @param	rounds	Maximial # of rounds. Use as few rounds as possible if rounds = 0.
	 * @param	amt		Changes of workload. New workload = origin workload + amt.
	 * @return	null
	 */
	void Adjust(int l, int r, int L, int R, int rounds = 0, int amt = 0);

	/**
	 * Send all adjust instructions to Rotary.
	 * @return	null
	 */
	void Over();
private:
	void add(int rnd, int appl, int src, int tar, int amt);
};
