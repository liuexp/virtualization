#include <iostream>
#include "RotaryRing.h"
#include "S.h"

RotaryRing::RotaryRing() {
	n = 0;
}

void RotaryRing::SetSM(const int n) {
	this->n = n;
	a.assign(n, 0);
	b.assign(n, 0);
}

void RotaryRing::Move(const int appl, const int source, const int target, int amount) {
	if (source != Null) {
		amount = std::min(a[source], amount);
		a[source] -= amount;
	}
	a[target] += amount;
	b[target] = appl;
}

void RotaryRing::Over() {
	// Do nothing.
}

std::vector<int> RotaryRing::Status() const {
	return a;
}

void RotaryRing::Pull(const int appl) {
	for (int i = 0; i < n; ++i)
		if (b[i] == appl) {
			a[i] = 0;
			b[i] = Null;
		}
	S.Interrupt();
}

void RotaryRing::Performance() const {
	std::cerr << "Performance:\n";
	std::cerr << "Excelent\n\n";
	std::cerr.flush();
}
