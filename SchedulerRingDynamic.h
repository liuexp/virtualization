#pragma once

#include "SchedulerRing.h"
#include "Application.h"
#include<vector>
#include<list>
#include<map>
#include<functional>
#include<algorithm>

typedef std::pair<int, int> Segment;
typedef std::list<Segment> SegList;
typedef std::vector<int> Args;
typedef std::vector<Args> Transaction;

class SchedulerRingDynamic : public SchedulerRing {
public:
	SchedulerRingDynamic();
	virtual void SetSM(const int);
	virtual void Push(const int, const int, const int, const int);
	virtual void Interrupt();
	virtual void Performance() const;

	// Utility functions
	virtual void Sync();
	virtual void updateStatus();
	virtual void dumpStatus();
	virtual void beginTransaction();
	virtual void commitTransaction();
	virtual void emitMove(const int,const int,const int,const int,const int);
	virtual Application* getAppBySeg(const Segment &) const;
	virtual SegList groupSeg() ;
	virtual SegList filterApp(const int) const;
	virtual SegList filterFree() const;
	virtual SegList filterNonFree() const;
	virtual inline int cirNext(int) const;
	virtual inline int cirPrev(int) const;

	// Strategy functions
	virtual void simpleMakeRoom(int, const int);
	virtual void rebalance(int,int,int,int,int,double);
	virtual void contract(Application *,Segment &s, const int, int &, double);


private:
	bool						__INITIALIZED__;
	int							n;			// # of SMs
	SegList						segList;	// segments list
	std::vector<int>			readyRound;	// the round when an SM is free
	std::vector<int>			statusLoad;	// load status of each SM
	std::vector<Transaction>	transactions;	//stores transactions
	std::vector<Application *>	statusApp;	// appId of each SM
	std::map<Application*,int> 	segcnt;		// segments count

	std::function<bool(const Segment &,const Segment &)> compSegLength; //asc
	std::function<bool(const Segment &,const Segment &)> compSegLengthNeg;
	//std::function<bool(const Segment &,const Segment &)> compSegLoad;
};
