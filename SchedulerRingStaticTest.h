#pragma once

#include "SchedulerRingStatic.h"
#include "Test.h"

class SchedulerRingStaticTest {
public:
	SchedulerRingStaticTest();
	void Run();
private:
	void Status(int = -1, std::ostream& os = std::cerr);
};
