#include <iostream>
#include "SchedulerRingStaticTest.h"
#include "R.h"
#include "S.h"

void cli(){
	int e;
	int n;
	std::cin>>n;
	S.SetSM(n);
	R.SetSM(n);
	while(std::cin>>e){
		if(e==0){
			int a,b,c,d;
			std::cin>>a>>b>>c>>d;
			S.Push(a,b,c,d);
		}else {
			int a;
			std::cin>>a;
			R.Pull(a);
		}
		//S.dumpStatus();
	}
}

int main() {
//	cli();
	SchedulerRingStaticTest t;
	t.Run();
}
