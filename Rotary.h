#pragma once

#include <vector>

class Rotary {
public:
	Rotary() {};
	virtual void Move(const int applID, const int sourceSMID, const int targetSMID, int amount) = 0;
	virtual void Over() = 0;
	virtual std::vector<int> Status() const = 0;
	virtual void Pull(const int applID) = 0;
	virtual void Performance() const = 0;
};
