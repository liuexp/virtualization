#include <iostream>
#include "SchedulerRingDynamic.h"
#include "R.h"
#include<functional>
#include<algorithm>
#include<vector>
#include<list>
#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<cassert>
#include<cstdlib>
#include<cassert>
#include<ctime>
#include<utility>
#include<fstream>

typedef std::pair<int, int> Segment;
typedef std::list<Segment> SegList;

void SchedulerRingDynamic::emitMove(const int a, const int m, const int o, const int l, const int r){
	Args z;
	z.push_back(a);
	z.push_back(m);
	z.push_back(o);
	z.push_back(l);
	int todo=r+1-transactions.size();
	for(int i=0;i<todo;i++){
		Transaction tmp;
		transactions.push_back(tmp);
	}
	transactions[r].push_back(z);
}

void SchedulerRingDynamic::beginTransaction(){
	transactions.clear();
}

void SchedulerRingDynamic::commitTransaction(){
	for (auto x : transactions) {
		for (auto y : x) {
			R.Move(y[0],y[1],y[2],y[3]);
		}
		R.Over();
	}
}

int cirLength(const Segment &x, const int n) {
	if(x.first > x.second) return 1 + (n - x.first + x.second);
	else return x.second - x.first + 1;
}

int SchedulerRingDynamic::cirNext(int x) const{
	return (x+1)%n;
}

int SchedulerRingDynamic::cirPrev(int x) const{
	return (x-1+n)%n;
}

SegList SchedulerRingDynamic::groupSeg() {
	SegList ret;
	Application *last=statusApp[0];
	ret.push_back(std::make_pair(0,0));
	for (int i = 1; i < n; i++) {
		if (last==statusApp[i]) {
			ret.back().second=i;
		}else {
			Segment t = std::make_pair(i,i);
			ret.push_back(t);
			last = statusApp[i];
		}
	}
	if (ret.size() >= 2 && getAppBySeg(ret.front()) == getAppBySeg(ret.back())) {
		ret.back().second = ret.front().second;
		ret.pop_front();
	}
	std::set<Application *> broken;
	segcnt.clear();
	std::priority_queue<Segment, std::vector<Segment>, decltype(compSegLength)> freeSeg(compSegLength);
	for (auto x : ret) {
		Application *a = statusApp[x.first];
		if(!a){
			freeSeg.push(x);
			continue;
		}
		if (segcnt.find(a) != segcnt.end()) {
			segcnt[a]++;
			if (a && a->minReq < segcnt[a]) {
				broken.insert(a);
			}
		}else {
			segcnt[a] = 1;
		}
	}
	std::set<Segment> toMerge;
	while(!freeSeg.empty()){
		auto x = freeSeg.top();
		freeSeg.pop();
		int ll = cirPrev(x.first);
		int rr = cirNext(x.second);
		if(ll == x.second || rr == ll)break;
		Application *al = statusApp[ll];
		Application *ar = statusApp[rr];
		if(al == ar && broken.find(al) != broken.end()) {
			toMerge.insert(x);
			segcnt[al]--;
			if (segcnt[al] <= al->minReq) {
				broken.erase(al);
			}
		}
	}
	if(ret.size()<=3)return ret;
	auto p=ret.begin(), q=p;
	q++;
	int i=0;
	int segsize = ret.size();
	while (i < segsize){
		if (toMerge.find(*p) != toMerge.end()) {
			assert(ret.size()>3);
			ret.back().second = q-> second;
			ret.pop_front();
			ret.pop_front();
			p=ret.begin();
			q=p;
			++q;
			i+=2;
		}else {
			ret.push_back(ret.front());
			ret.pop_front();
			i++;	
		}
	}
	return ret;
}

SegList SchedulerRingDynamic::filterNonFree() const{
	SegList ret;
	for (auto x : segList) {
		if (getAppBySeg(x)) {
			ret.push_back(x);
		}
	}
	return ret;
}

SegList SchedulerRingDynamic::filterFree() const{
	SegList ret;
	for (auto x : segList) {
		if (!getAppBySeg(x)) {
			ret.push_back(x);
		}
	}
	return ret;
}

SegList SchedulerRingDynamic::filterApp(const int appId) const{
	SegList ret;
	for (auto x : segList) {
		Application *a = getAppBySeg(x);
		if (a->appid == appId) {
			ret.push_back(x);
		}
	}
	return ret;
}

Application* SchedulerRingDynamic::getAppBySeg(const Segment &x) const{
	for (int i = x.first; ; i=cirNext(i)) {
		if (statusApp[i]) {
			return statusApp[i];
		}
		if(i==x.second)break;
	}
	return NULL;
}

SchedulerRingDynamic::SchedulerRingDynamic() {
	__INITIALIZED__ = false;
	n = -1;
}

void SchedulerRingDynamic::SetSM(const int n) {
	this->n = n;
	this-> __INITIALIZED__ = true;
	compSegLength = [n](const Segment &a, const Segment &b) -> bool { return cirLength(a, n) < cirLength(b, n);};
	compSegLengthNeg = [n](const Segment &a, const Segment &b) -> bool { return cirLength(a, n) > cirLength(b, n);};
	for (int i = 0; i < n; i++) {
		readyRound.push_back(0);
		statusLoad.push_back(0);
		statusApp.push_back(NULL);
	}
	srand(time(NULL));
}

void SchedulerRingDynamic::updateStatus(){
	std::set<Application *> affectedApp;
	std::set<Application *> allApp;
	for (int i = 0; i < n; i++) {
		if(statusApp[i])statusApp[i]->occupiedSM = 0;
		if (statusLoad[i] == 0) {
			if(statusApp[i])affectedApp.insert(statusApp[i]);
			statusApp[i] = NULL;
		}else {
			allApp.insert(statusApp[i]);
		}
		readyRound[i] = statusApp[i]?-1:0;
	}
	for (auto x : allApp) {
		affectedApp.erase(x);
	}
	for (auto x : affectedApp) {
		delete x;
	}
	segList = groupSeg();
	for (auto x : segList) {
		Application *a = getAppBySeg(x);
		if (a) {
			a->occupiedSM += cirLength(x, n);
		}
	}

}

void SchedulerRingDynamic::Sync(){
	assert(__INITIALIZED__);
	statusLoad=R.Status();
	updateStatus();
	beginTransaction();
}

void SchedulerRingDynamic::dumpStatus(){
	//std::ofstream slog("slog");
#define slog std::cerr
	slog<<std::endl<<std::endl;
	auto prettyPrint = [](int x){
		std::string ret;
		for (int i = 0; i < x; i++) {
			ret+="#";
		}
		return ret;
	};
	for (int i = 0; i < n; i++) {
		slog<<i<<"\t:[";
		Application *a = statusApp[i];
		if(a){
			//slog<<a->toString()<<","<<statusLoad[i];
			slog<<a->toString()<<","<<(prettyPrint(statusLoad[i]));
		}
		slog<<"]"<<std::endl;
	}
	slog<<transactions.size()<<std::endl;
	for (int i = 0; i < int(transactions.size()); i++) {
		slog<<transactions[i].size()<<std::endl;
		for (auto x : transactions[i]) {
			int a=x[0],b=x[1],c=x[2],d=x[3];
			slog<<a<<" "<<b<<" "<<c<<" "<<d<<std::endl;
		}
	}
}

void SchedulerRingDynamic::Push(const int appId, const int minReq, const int optReq, const int load) {
	assert(__INITIALIZED__);
	assert(minReq > 0);
	assert(load >= optReq);
	Sync();
	auto freeSegList = filterFree();
	freeSegList.sort(compSegLengthNeg);
	Application *a=new Application(appId,minReq,optReq,load);
	int i=0;
	int topFreeSM=0;
	auto updateTopFreeSM = [&](){
		for(auto x: freeSegList){
			if(i>=minReq||topFreeSM>=optReq)break;
			topFreeSM+=cirLength(x,n);
			i++;
		}
		topFreeSM = std::min(topFreeSM, optReq);
	};
	updateTopFreeSM();
	if(topFreeSM<minReq){ //implies freeSegList.size()<min_req
		simpleMakeRoom(minReq-topFreeSM,-1);
		updateStatus();
		freeSegList=filterFree();
		freeSegList.sort(compSegLengthNeg);
		i=0;
		updateTopFreeSM();
	}
	assert(topFreeSM >= minReq);
	int assignedLoad=0;
	int curLoad=load / topFreeSM;
	bool flag=assignedLoad>=load;
	std::set<int> toTouch;
	auto spawnThread = [&](int addLoad){
		i=0;
		for(auto &x : freeSegList){
			if(i>=minReq)break;
			int l=x.first,r=x.second;
			for (int y = l;	; y=cirNext(y)) {
				if(!statusApp[y]){
					if(a->occupiedSM>=optReq)break;
					a->occupiedSM++;
				}
				statusApp[y]=a;
				statusLoad[y]+=addLoad;
				assignedLoad+=addLoad;
				toTouch.insert(y);
				flag=assignedLoad>=load;
				if(flag)return;
				if(y==r)break;
			}
			i++;
		}
	};
	spawnThread(curLoad);
	while (!flag) {
		spawnThread(1);
	}
	for (auto x : toTouch) {
		emitMove(appId, R.Null, x, statusLoad[x], readyRound[x]);
	}
	commitTransaction();
}

//TODO:looks like not fully expanded
void SchedulerRingDynamic::Interrupt() {
	assert(__INITIALIZED__);
	Sync();
	SegList rSegList = segList;
	int segsize = rSegList.size();
	if(segsize<2)return;
	for (int i = 0; i < segsize;) {
		auto q = rSegList.front();
		rSegList.pop_front();
		Application *a = getAppBySeg(q);
		if(!a){
			int sL = cirLength(q,n);
			int round = std::min((sL+1)>>1,4);

			auto p = rSegList.back();
			int pL = cirLength(p,n);
			Application *ap = getAppBySeg(p);

			auto r = rSegList.front();
			int rL = cirLength(r,n);
			Application *ar = getAppBySeg(r);

			auto getLoad = [&](Segment &x){
				int load=0;
				for (int i = x.first;; i=cirNext(i)) {
					load+=statusLoad[i];
					if(i==x.second)break;
				}
				return load;
			};
			//int rRound = std::min(round, sL - round);
			int rRound = round;
			double pload = getLoad(p) /double(pL+2*round),
				   rload = getLoad(r) /double(rL+2*rRound);
			//simpleRebalance(ap->appid, p.first,(p.second+round)%n,p.second+round,round,pload);
			//simpleRebalance(ar->appid, (n+r.first-rRound)%n,r.second,(n+r.first-rRound)%n,rRound,rload);
			rebalance(ap->appid, p.first,(p.second+round)%n,(p.second+round)%n,round,pload);
			rebalance(ar->appid, (n+r.first-rRound)%n,r.second,(n+r.first-rRound)%n,rRound,rload);
		}else {
			i++;
			rSegList.push_back(q);
		}
		
	}
	updateStatus();
	commitTransaction();
}

void SchedulerRingDynamic::Performance() const {
	assert(__INITIALIZED__);
	std::cerr << "Performance:\n";
	std::cerr << "Excelent\n\n";
	std::cerr.flush();
}

void SchedulerRingDynamic::rebalance(int appId, int l, int r, int target, int round, double load){
	assert((r-l)*(r-target)*(target-l)>=0);
	if (l == r) return;
	double expecting = (load - statusLoad[target]);
	auto doRebalance = [&](int p, int np, int curRound){
		int todo = std::min((int)floor(expecting), statusLoad[p]); //TODO: further tune here.
		double delta = load - statusLoad[p];
		if(todo>0) {
			emitMove(appId,p,np,todo,curRound);
			statusLoad[p] -= todo;
			statusLoad[np] += todo;
		}
		if(!statusApp[np])statusApp[np] = statusApp[p]; //TODO: should we update occupiedSM here or invoke updateStatus() later?
		expecting += delta;
	};
	for(int curRound=0;curRound<round;curRound++){
		if(target!=l){
			expecting = load - statusLoad[target];
			if(target != r) expecting = expecting * 0.5;
			for(int p=cirPrev(target);;p=cirPrev(p)){
				int np=cirNext(p);
				doRebalance(p, np, curRound);
				if(p==l)break;
			}
		}
		if(target==r)continue;
		expecting = load - statusLoad[target];
		for(int p=cirNext(target);;p=cirNext(p)){
			int np=cirPrev(p);
			doRebalance(p, np, curRound);
			if(p==r)break;
		}
	}
}

//TODO: segment at the center instead of centroid? or segment at the centroid but also assign todos respectively?
void SchedulerRingDynamic::contract(Application *a, Segment &s, int todo, int &maxSegCnt, double load){
	int appid = a->appid;
	int sL=cirLength(s,n);
	int cnt = sL - todo;
	
	if(todo<=0)return;
	const int l=s.first,r=s.second;

	//try further segment
	if(maxSegCnt>0&&sL>=8&&todo>=4&&cnt>=2){
		//first calculate the mass centroid.
		double p=0,q=0;
		for(int j=l;;j++){
			int tmp=statusLoad[j%n];
			q+=tmp;
			p+=j*tmp;
			if(j%n == r)break;
		}
		int centroid=int(floor(p/q)) % n;
		//now segment at the centroid.
		Segment ls,rs;
		ls=std::make_pair(l,centroid);
		rs=std::make_pair(cirNext(centroid),r);
		maxSegCnt--;
		segcnt[a]++;

		int lsegCnt=maxSegCnt>>1,rsegCnt=maxSegCnt-lsegCnt;
		int lsL=cirLength(ls,n),rsL=cirLength(rs,n);
		int ltodo = std::max(0, std::min(lsL-1, todo>>1)), rtodo = std::min(todo - ltodo, rsL-1);
		if(ltodo + rtodo < todo){
			if(ltodo < lsL-1)ltodo = std::min(lsL - 1, todo - rtodo);
			else if(rtodo < rsL-1)rtodo = std::min(rsL - 1, todo - ltodo);
			else assert(false); // serious data inconsistency.
		}
		assert(ltodo + rtodo <= todo);

		//recursive contraction
		contract(a,ls,ltodo,lsegCnt,load);
		contract(a,rs,rtodo,rsegCnt,load);

		maxSegCnt=lsegCnt+rsegCnt;
		assert(maxSegCnt + segcnt[a] <= a->minReq);
		return;
	}
	//do rebalance
	
	if(load>0&&cnt>=5){
		int round=std::max(((todo+1)>>1) ,1);
		int center = (l + (cirLength(s,n)>>1)) %n;
		rebalance(appid,l,r,center,round,load);
	}
	//begin contraction
	int i=0;
	bool flag=(rand()%2==0);
	for(int id=l;;id=cirNext(id)){
			if(flag){
				if(i*2>=todo)break;
			}else{
				if(i*2>=todo-1)break;
			}
			//if(statusLoad[id]>0)
			emitMove(appid, id, cirNext(id), statusLoad[id], i);
			statusLoad[cirNext(id)]+=statusLoad[id];
			statusLoad[id]=0;
			//should this be i or i+1?
			readyRound[id]=i;
			i++;
			if(id==r)break;
	}
	todo-=i;
	i=0;
	for(int id=r;;id=cirPrev(id)){
			if(i>=todo)break;
			//if(statusLoad[id]>0)
			emitMove(appid, id, cirPrev(id), statusLoad[id], i);
			statusLoad[cirPrev(id)]+=statusLoad[id];
			statusLoad[id]=0;
			//should this be i or i+1?
			readyRound[id]=i;
			i++;
			if(id==l)break;
	}
}

void SchedulerRingDynamic::simpleMakeRoom(int cnt, const int exception){
	if(cnt<=0)return;
	SegList nonFree = filterNonFree();
	typedef std::pair<double, std::pair<int, Segment > > WeightedSegment;
	std::priority_queue< WeightedSegment > pq;
	std::map<Segment, int> brokenSeg;
	for (auto x : nonFree) {
		double curLoad=0;
		int broken=0;
		for (int i = x.first; ; i=cirNext(i)) {
			curLoad+=statusLoad[i];
			if(statusLoad[i] == 0)
				broken++;
			if(i==x.second)break;
		}
		if(broken > 0) brokenSeg[x]=broken;
		curLoad = cirLength(x,n) / curLoad;
		pq.push(std::make_pair(curLoad, std::make_pair(cirLength(x,n), x)));
	}
	std::list<WeightedSegment> todoList;
	while(cnt>0){
		auto tmp = pq.top();
		pq.pop();
		double perf = tmp.first;
		int segLength = tmp.second.first;
		Segment &s = tmp.second.second;
		Application *a = getAppBySeg(s);
		if(a->appid == exception)continue;
		if(a->occupiedSM > a->minReq && segLength>1){
			pq.push(std::make_pair(perf*(segLength-1)/segLength, std::make_pair(segLength-1,s)));
			cnt--;
			a->occupiedSM--;
		}else {
			todoList.push_back(tmp);
		}
	}
	while(!pq.empty()||!todoList.empty()) {
		WeightedSegment tmp;
		if (!pq.empty()) {
			tmp= pq.top();
			pq.pop();
		}else {
			tmp = todoList.front();
			todoList.pop_front();
		}
		Segment &s = tmp.second.second;
		Application *a = getAppBySeg(s);
		int curMaxSeg = a->minReq - segcnt[a];
		int sL=tmp.second.first;
		int todo = cirLength(s, n) - sL;
		contract(a, s,todo,curMaxSeg,1/tmp.first);
		brokenSeg.erase(s);
	}
	int maxRound = transactions.size();
	while(!brokenSeg.empty()) {
		auto tmp=*brokenSeg.begin();
		Segment s = tmp.first;
		Application *a = getAppBySeg(s);
		int curMaxSeg = a->minReq - segcnt[a];
		int todo = std::min(tmp.second, maxRound<<1);
		contract(a, s, todo, curMaxSeg, -1);
		brokenSeg.erase(brokenSeg.begin());
	}
}
