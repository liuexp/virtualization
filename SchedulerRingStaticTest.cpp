#include "SchedulerRingStaticTest.h"

SchedulerRingStaticTest::SchedulerRingStaticTest() {
}

void SchedulerRingStaticTest::Status(int m, std::ostream& os) {
	bool flag = true;
	for (int i = 0; flag && i < S.n; ++i)
		flag &= S._crt[i] == -1;
	if (flag) {
		os << "Empty!\n";
		return;
	}
	if (m & 1) {
		for (int i = 0; i < S.n; ++i)
			if (S._crt[i] != -1) os << "|" << S._crt[i] << "| ";
			else os << " -  ";
		os << "\n--------------------------------------------------------------------------------\n";
	}
	if (m & 2) {
		for (int i = 0; i < S.n; ++i)
			if (S._crt[i] != -1) os << "|" << S._aff[i] << "| ";
			else os << " " << S._aff[i] << "  ";
		os << "\n--------------------------------------------------------------------------------\n";
	}
	if (m & 4) {
		for (int i = 0; i < S.n; ++i)
			if (S._crt[i] != -1) os << "|" << S._load[i] << "| ";
			else os << " " << S._load[i] << "  ";
		os << "\n--------------------------------------------------------------------------------\n";
	}
	if (m == -1) {
		//system("pause");
		system("read -n 1");
	}
}

void SchedulerRingStaticTest::Run() {
	int SM = 256;
	R.SetSM(SM);
	S.SetSM(SM);
	S.Push(1, 10, 20, 10000);
	Status();
	S.Interrupt();
	Status();
	S.Push(2, 10, 20, 10000);
	Status();
	S.Push(3, 10, 20, 10000);
	Status();
	S.Push(4, 10, 20, 10000);
	Status();
	R.Pull(4);
	Status();
	R.Pull(3);
	Status();
	R.Pull(2);
	Status();
	R.Pull(1);
	Status();
};
