#pragma once

#include "SchedulerRing.h"

class SchedulerRingStatic : public SchedulerRing {
	friend class SchedulerRingStaticTest;
public:
	SchedulerRingStatic();
	virtual void SetSM(const int);
	virtual void Push(const int, const int, const int, const int);
	virtual void Interrupt();
	virtual void Performance() const;
private:
	std::vector<int> _crt;
	int& crt(int);

	int u(int, int = -1);
	int v(int, int = -1);
	int U(int, int = -1);
	int V(int, int = -1);
};
